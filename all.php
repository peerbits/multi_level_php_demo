<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
     border: 1px solid black;
}
</style>
</head>
<body>

<?php
include 'db.php';

$sql = "SELECT id, name, parent_id FROM user_master";
	
$result = $conn->query($sql);

if ($result->num_rows > 0) {
     echo "<table><tr><th>ID</th><th>Name</th><th>Parent ID</th></tr>";
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo "<tr><td>" . $row["id"]. "</td><td>" . $row["name"]. "</td><td>" . $row["parent_id"] . "</td></tr>";
     }
     echo "</table>";
} else {
     echo "0 results";
}

	
$conn->close();
?>  

</body>
